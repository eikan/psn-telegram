#!/usr/local/bin/python2.7
## -*- coding: utf-8 -*-

#
#sqlite3 psn-hidden.db "update psn set telegram = 'Immelstorn' where psnid='ImmelstornRU';";
#

import json
import requests
import sqlite3
import datetime
import psnurlconfig
import myos
import re
import html
from pprint import pprint
from multiprocessing.dummy import Pool as ThreadPool
from uuid import uuid4
import random

import sys

reload(sys)  
sys.setdefaultencoding('utf8')

DEBUG = 0
psnids = psnurlconfig.psnids
TOKEN = psnurlconfig.TOKEN
GUMER = psnurlconfig.GUMER
DB_FILE = psnurlconfig.DB_FILE
CHATID = psnurlconfig.CHATID
#CHATID = '41788920'
MARTATOKEN = psnurlconfig.MARTATOKEN
WWW = psnurlconfig.WWW
DB_FILE = '/home/gor/py/psn-hidden.db'
#DB_FILE = '/home/gor/py/psn-hidden-test.db'
HIDDEN_FILE = '/home/gor/py/hidden-trophies'
TROPHY_TEMP = '/tmp/psn-trophy'
spamers = ['Kot_chame1eon', 'spacentropy', 'xndr__78', 'Sadness_5m', 'nalorokk', 'johnylemonade', 'Shatohin', 'Djunno', 'timonua', 'R4mboSTR','Sundelor', 'CERBER-FALLEN', 'grayhex', 'Orr1n-_-', 'dead_m1nd', 'eXceptoRR', 'PerdakDestroyer', 'lesharodin', 'DanilaRU', 'Nalorokk', 'RubyRoid', 'Sunde1or', 'kot_chame1eon', 'Pinkertop', 'sadness_5m', 'Yushkevich', 'stupid_fat_hare']
#spamers = ['Kot_chame1eon', 'spacentropy', 'xndr__78', 'Sadness_5m', 'nalorokk', 'johnylemonade', 'Shatohin', 'Djunno', 'timonua', 'R4mboSTR','Sundelor', 'CERBER-FALLEN', 'grayhex', 'Orr1n-_-', 'dead_m1nd', 'eXceptoRR', 'PerdakDestroyer', 'lesharodin', 'Ironshell_thor', 'schnappi_omsk']
fireteam = ['gorcheque', 'simpletonlj', 'Pinkertop', 'keselmanio']


dks_stickers = ['BQADAgADOgAD5R-VArXR5RkKXxxDAg',
    'BQADAgADNgAD5R-VAhBnDYDdKueDAg',
    'BQADAgAD7gAD5R-VAn67AAFR5koPbQI',
    'BQADAgADSwAD5R-VAs5hmwABqX-T5wI',
    'BQADAgADPAAD5R-VAjmWY-22-gbRAg',
    'BQADAgADVQAD4djSAAHrx77rnoaAswI',
    'BQADAgAD3gAD5R-VAioQX7xTx03eAg',
    'BQADAgAD9AAD5R-VAmFwI7CeCmHBAg',
    'BQADAgAD8gAD5R-VAnU94hGqkblmAg',
    'BQADAgADbAAD5R-VAk5W6kMRMWP9Ag',
    'BQADAgAD3gAD5R-VAioQX7xTx03eAg',
    'BQADAgADZgAD5R-VAu10wXl_VbPfAg',
    'BQADAgADVwAD5R-VAts3F61lhfbqAg',
    'BQADAgADYAAD5R-VAmt0Dj8MOwFNAg']

#psnids = ['gorcheque']
#psnids = ['schnappi_omsk']
#psnids = ['ergo_guilty']
#psnids = ['dukensk']
#psnids = ['Kot_chame1eon']
#psnids = ['No_OTime']

def telegram (message):
    #url = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?parse_mode=markdown&chat_id=' + CHATID + '&text=' + message
    url = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?parse_mode=html&chat_id=' + CHATID + '&text=' + message + '&disable_notification=True'
    #print url
    try:
        r = requests.get(url)
        #print ('a')
    except requests.ConnectionError:
        print "Telegram connection error"
        return 0

    #send to channel 
    url = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?parse_mode=html&chat_id=@leprapsn&text=' + message + '&disable_notification=True'
    r = requests.get(url)


    now = datetime.datetime.now()
    date = now.strftime("%Y-%m-%d %H:%M")
    with open ('/home/gor/py/psn-ach.log', 'a') as out:
        out.write(date + ': ' + message + '\n')
        out.close()

def telegram_fireteam (message):
    #message to DNO team
    url = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?parse_mode=html&chat_id=-1001110538336&text=' + message + '&disable_notification=True'
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        print "Telegram connection error"
        return 0

def telegram_marta (message):
    #message to nogpyra channel

    url = 'https://api.telegram.org/bot' + MARTATOKEN + '/sendMessage?parse_mode=html&chat_id=-1001034176133&text=' + message + '&disable_notification=True'
    #print url
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        print "Telegram connection error"
        return 0

def telegram_darksouls (message):
    url = 'https://api.telegram.org/bot' + TOKEN + '/sendMessage?parse_mode=html&chat_id=-1001076600464&text=' + message + '&disable_notification=True'
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        print "Telegram connection error"
        return 0

def sticker (file_id):
    url = 'https://api.telegram.org/bot' + TOKEN + '/sendSticker?chat_id=' + CHATID + '&sticker=' + file_id
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        print "Telegram connection error"
        return 0


def get_hidden(npwr, trophy_id):
    print "GET HIDDEN START"

    #print ("trophy_id: " + str(trophy_id))
    print ("Don't forget to realise update for hidden trophies file")
    #print npwr
    out = myos.run ("/bin/grep " + npwr + " " + HIDDEN_FILE)
    #print (out)
    url = re.search (r'href="(\S+)"', out).group(1)
    #/trophies/07028-uncharted-4-a-thief’s-end
    #print (url)

    #for_grep = url.split('/')[2] + '/' + str(trophy_id)
    for_grep = url.split('/')[2] + '/' + str(trophy_id) + '-'
    #print ("for grep: " + for_grep)
    url = 'http://ps3trophies.com' + url + '/&s=xmb'
    print (url)

    cmd = "wget -q -O " + TROPHY_TEMP + " " + url
    myos.run(cmd)

    #<img alt="Bronze" src="http://www.ps3trophies.com/images/games/NPWR07028/trophies/46S6dc5eb.png" style="opacity:0.5;filter:alpha(opacity=50);-moz-border-radius: 3px;-webkit-border-radius: 3px;border-radius: 3px;">
    #print  ("cat " + TROPHY_TEMP + "|grep -v 'Platinum Club'|grep -A 2 " + for_grep + "|grep img")
    out = myos.run ("cat " + TROPHY_TEMP + "|grep -v 'Platinum Club'|grep -A 2 " + for_grep + "|grep img")
    #print out
    icon = re.search(r'.*src="(\S+)".*', out).group(1)
    #print out
    #alt="Bronze"
    trtype = re.search(r'.*alt="(\S+)".*', out).group(1)
    print trtype

    #<a href="/trophy/07028-uncharted-4-a-thief’s-end/50-glamour-shot"><span class="title">Glamour Shot</span></a>
    out = myos.run ("cat " + TROPHY_TEMP + "|grep -v 'Platinum Club'|grep -A 2 " + for_grep + "|grep title")
    title = re.search(r'.*title">(.*?)</.*', out).group(1)
    
    #<span class="description">Just prior to City Chase, stand perfectly still for 30 seconds (stage demo fail)</span>
    out = myos.run ("cat " + TROPHY_TEMP + "|grep -v 'Platinum Club'|grep -A 2 " + for_grep + "|grep description")
    print out
    try:
        descr = re.search(r'.*description">(.*?)</.*', out).group(1)
    except:
        descr = re.search(r'.*description">(.*?)', out).group(1)



    #out = myos.run ("/bin/grep " + 

    return (icon, title, descr, trtype.lower())

def last_trophy (psnid):

    print (psnid)
    #на 3000 порту у нас запущено https://github.com/jhewt/gumer-psn
    psn_host = GUMER
    url = psn_host + psnid + '/trophies'
    #print url
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        print "Connection error"
        return 0

    if r.status_code == 200:
        try:
            data = json.loads(r.text)
        except:
            print "ERROR: GUMER returns 'Rate limit exceeded'.\nTry to decrease 'pool = ThreadPool(16)'"
            return 0

    #pprint (data)
    try:
        npwr = data['trophyTitles'][0]['npCommunicationId'].split('_')[0]
    except:
        print "ERROR: GUMER returns 'Rate limit exceeded'"
        return 0


    sorted_npwr = {}
    for i in data['trophyTitles']:
        #pprint(i)
        try:
            npwr = i['npCommunicationId'].split('_')[0]
            #print npwr
            game = i['trophyTitleName']
            #print game
            date = i['comparedUser']['lastUpdateDate']
            #print date
        except:
            print "ERROR: GUMER returns 'Rate limit exceeded'"
        sorted_npwr[date] = npwr, game

    for i in sorted (sorted_npwr, reverse = True):
        new_npwr = sorted_npwr[i][0]
        new_game = sorted_npwr[i][1]
        break

    npwr = new_npwr
    
    #game =  data['trophyTitles'][0]['trophyTitleName'].replace(u'\u2122','').encode('utf-8')
    gamea =  new_game.replace(u'\u2122','')
    gameb =  gamea.replace(u'Призы ','').encode('utf-8')
    #print "ZZZZZZ" + gameb
    game =  gameb.replace(u'&','%26')
    game =  game.replace(u'\u00ae','')

    #print data['trophyTitles'][0]['trophyTitleDetail']
    #game =  data['trophyTitles'][0]['trophyTitleName']

    url = psn_host + psnid + '/trophies/' + new_npwr + '_00'
    print url
    try:
        r = requests.get(url)
    except requests.ConnectionError:
        print "ERROR: Connection error"
        return 0

    if r.status_code == 200:
        data = json.loads(r.text)

    #pprint(data)

    #structure with useful data
    #d[trophyId] = [trophy_time, i['trophyDetail'], i['trophyType'], i['trophyName']]
    d = {}

    #list with dates (earned time) only for finding maximum date
    dates = []

    #print("COMMENT NEED AN UPDATE")
    #list of dicts of list [fromUser['onlineId'], game, 'hidden', i, 'hidden'] of maximum dates
    #usially maximum date is unique (and function returns list with one d), but when player gets platinum, dates of last achievement and platinum are the same, so we should return both

    trophy_time = ''

    try:
        tmp = data['trophies']
    except:
        print "ERROR: in data['trophies']"
        return 0

    #в json 'data' передаётся весь список ачивок для конкретной игры, даже не полученных конкретным psnid
    for i in data['trophies']:
        fromUser = i['comparedUser']

        #если у пользователя есть эта ачивка, то earned=true
        if fromUser['earned']:
            try:
                trophy_time = fromUser['earnedDate']
            except:
                pass
            trophyId = i['trophyId']
                

            #Если есть полученная платина, то сразу отсылаем её
            #if not i['trophyHidden'] and i['trophyType'] == 'platinum':
            #    return [game, i['trophyDetail'], trophy_time, i['trophyType'], i['trophyName']]

            #если ачивка скрыта, сохраняем 'hidden' вместо деталей и типа
            #print (trophy_time)
            dates.append(trophy_time)
            hidden = i['trophyHidden']
            trophy_id = i['trophyId']
            trophyEarnedRate = i['trophyEarnedRate']
            if hidden:
                d[trophyId] = [trophy_time, 'hidden','hidden','hidden', game, 'noticon', trophy_id + 1, trophyEarnedRate]
            else:
                iconurl = i['trophyIconUrl'].encode('utf-8')
                d[trophyId] = [trophy_time, i['trophyDetail'].encode('utf-8'), i['trophyType'], i['trophyName'].encode('utf-8'), game, iconurl, trophy_id, trophyEarnedRate]

    #pprint(d)

    #когда пользователь впервые запускает игру, npCommunicationId меняется на неё, не смотря на то, что еще не получено ни одной ачивки, поэтому ничего не делаем
    #при желании в этом месте можно вывести "впервые играет в" (не забыть сохранить в БД)
    if not len(d):
        print (psnid + ": Первый запуск")
        return 0

    max_date = max(dates)
    #print (max_date)

    #did we find any date later then date in DB?
    anyluck = 0
    ach_count = 0
    lastnpwr = ''

    #for i in d:
    for i in sorted(d, reverse = True):
        #print(d[i][0])
        #we check every date if that bigger than date in DB,
        #send it to chat and change in DB to max_date

        #print ("New date: " + d[i][0])
        #print ("Stored    date: " + stored_trophies[psnid][2])

        if stored_trophies[psnid][2] < d[i][0]:
            anyluck = 1
            #pprint(d[i])

            #here we can add limit for trophies to send at the same time
            ach_count = ach_count + 1

            det = d[i][1]
            details = det.replace('#','N')
            details = details.replace(u'\u00ae','')
            trophy_type = d[i][2]
            #print ("trophy_type {}".format(trophy_type))
            trophy_name = d[i][3]
            iconurl = d[i][5]
            telegramnick = tlgrm[psnid].encode('utf-8')
            rarity = d[i][7]

            #for each max_date we check if new date is differ than date in DB
            #and send telegram message and change date in DB 
            #if 1:
            if (ach_count > 2 and psnid in spamers):
                print ("SPAMER " + psnid)
            if not (ach_count > 2 and psnid in spamers):
                print ("New max date is late then stored. Sending to telegram and updating DB")
                print ("Stored date:\t{}\nNew date:\t{}".format(stored_trophies[psnid][2], d[i][0]))
                print ("type {}, name {}, game {}".format(trophy_type, trophy_name, game))

                if trophy_type == 'hidden':
                    #message  = "<b>%s</b> получил скрытую ачивку в <b>%s</b>" % (psnid, game)
                    #message  = "<b>%s</b> в <b>%s</b> (<code>скрытая</code>)" % (psnid, game)
                    (hid_icon, hid_title, hid_descr, hid_type) = get_hidden (npwr, d[i][6])
                    print ("HIDDEN INFO:")
                    print (hid_icon, hid_title, hid_descr, hid_type)

                    #trophy_name = hid_title + ' [hidden]'
                    trophy_name = hid_title  + ' (hidden)'
                    details = hid_descr
                    iconurl = hid_icon
                    #ach_type = 'скрытая'
                    #ach_type = 'hidden'
                    ach_type = hid_type
                    trophy_type = hid_type
                    #message  = "<b>%s</b> в <b>%s</b> (скрытая)" % (psnid, game)
                    #telegram(message)

                if (1): 
                    if trophy_type == 'bronze':
                        #ach_type = 'бронза'
                        ach_type = 'bronze'
                    if trophy_type == 'silver':
                        #ach_type = 'серебро'
                        ach_type = 'silver'
                    elif trophy_type == 'gold':
                        #ach_type = 'ЗОЛОТО'
                        ach_type = 'GOLD'
                    elif trophy_type == 'platinum':
                        #ach_type = '<b>ПЛАТИНА</b>'
                        ach_type = '<b>PLATINUM</b>'

                    #message =  "*%s* получил %sачивку '*%s*' (%s) в *%s*" % (psnid, ach_type, trophy_name, details, game)
                    #message =  "*%s* получил %sачивку '*%s*' (%s) в *%s* - %s" % (psnid, ach_type, trophy_name, details, game, iconurl)

                    #generate html file with trophy_name, details and iconurl
                    #rndfile = str (uuid4()) + ".html"
                    rndfile = str (uuid4())[:6] + ".html"
                    filename = WWW + rndfile
                    with open (filename, 'a') as out:
                        quote_trophy_name = trophy_name.replace("'", '&apos;')
                        quote_details = details.replace("'", '&apos;')

                        content = "<!DOCTYPE html> \
                        <html xmlns:cc='http://creativecommons.org/ns#'> \
                        <head prefix='og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#'> \
                        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' integrity='sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u' crossorigin='anonymous'> \
                        <meta name='viewport' content='width=device-width, initial-scale=1'> \
                        <meta content='text/html; charset=utf-8' http-equiv='Content-Type'> \
                        <meta content='" + quote_trophy_name + "' name='title'> \
                        <meta content='" + quote_details + "<br>(" + ach_type + " — " + rarity + "%) ' name='description'> \
                        <meta content='" + iconurl + "' property='og:image'> \
                        </head> \
                        <body style='padding: 0px 0px 5px 20px;'> \
                        <h1><a href='http://psnprofiles.com/" + psnid + "'>" + psnid + "</a><br> \
                        <a href='http://igornet.ml/psn/top.html'>TOP</a></h1> \
                        <h4><a href='https://www.stratege.ru/site_search#args:ajax=1&queryfr=" + game + "'>Искать на stratege.ru</a></h4> \
                        </h1> \
                        <img src='" + iconurl + "'> \
                        </body> \
                        </html>"
                        out.write(content)
                        out.close()

                    #<b>gorcheque</b> получил серебряную ачивку в <b>Until Dawn</b>
                    #message = "<a href='http://psnbot.tk/psn/" + rndfile + "'>" + psnid + "</a> в <b>" + game + "</b> (<code>" + ach_type + "</code>)"
                    #message = "<a href='http://psnbot.tk/psn/" + rndfile + "'>" + psnid + "</a> в <code>" + game + "</code> (<code>" + ach_type + "</code>)"
                    #message = "<a href='http://psnbot.tk/psn/" + rndfile + "'>" + telegramnick + "</a> в <b>" + game + "</b> (" + ach_type + ")"
                    #message = "<a href='http://psnbot.tk/psn/" + rndfile + "'>" + telegramnick + "</a> в <b>" + game + "</b> (" + ach_type + ")"
                    #message = "@" + telegramnick + " <a href='http://psnbot.tk/psn/" + rndfile + "'>—</a> <b>" + game + "</b> (" + ach_type + ")"
                    #message = "@" + telegramnick + " <a href='http://psnbot.tk/psn/" + rndfile + "'>—</a> <b>" + game + "</b> (" + ach_type + ")"
                    #message = "@" + telegramnick + " " + game + " (<a href='http://psnbot.tk/psn/" + rndfile + "'>" + ach_type + "</a>)" 
                    #message = "<b>@" + telegramnick + " " + game + "</b><a href='http://psnbot.tk/psn/" + rndfile + "'>.</a>" 
                    #message = "@" + telegramnick + " " + "<a href='http://psnbot.tk/psn/" + rndfile + "'>" + game + "</a>" 
                    message = "@" + telegramnick + " — " + "<a href='http://igornet.ml/psn/" + rndfile + "'>" + game + "</a>" 
                    print (message)
                    telegram(message)

                    lastnpwr = npwr

                    if trophy_type == 'platinum':
                        sticker('BQADAgADTwsAAkKvaQABElnJclGri9EC')
                        #sticker(sendSticker?chat_id=41788920&sticker=BQADAgADTwsAAkKvaQABElnJclGri9EC"

                    #sending dark souls stickers
                    if ach_count == 1 and (npwr == 'NPWR07897' or npwr == 'NPWR08199' or npwr == 'NPWR00817' or npwr == 'NPWR13281'):
                        #sticker('BQADAgADOgAD5R-VArXR5RkKXxxDAg')
                        sticker(random.choice(dks_stickers))

                    #ачивки из Dark Souls отсылаем также в дс-чатик
                    if npwr == 'NPWR07897' or npwr == 'NPWR08199' or npwr == 'NPWR00817' or npwr == 'NPWR05818' or npwr == 'NPWR01249' or npwr == 'NPWR13281':
                        telegram_darksouls(message)

                    if (psnid in fireteam) and (npwr == 'NPWR04936'):
                        telegram_fireteam(message)
                    #
                    if psnid == 'nogpyraa':
                        print "MARTA"
                        telegram_marta(message)


                #saving new date to DB
                cur = con.cursor()
                cur.execute(
                    'UPDATE psn SET game = ?, details = ?, time = ?, type = ? where psnid = ?',
                    (game.decode('utf-8'), details.decode('utf-8'), max_date, trophy_type, psnid.decode('utf-8'))
                    #(game, details, d[i][0], trophy_type, psnid.decode('utf-8'))
                    )
                con.commit()

    #did we find any date later then date in DB?
    if anyluck:
        #saving lastnpwr to web
        filename = WWW + 'lastnpwr'
        with open (filename, 'w') as out:
            out.write(lastnpwr)
            out.close()
        print ("Date are not same")
        short_npwr = lastnpwr[4:]
        filename = WWW + 'npwr/' + short_npwr
        with open (filename, 'w') as out:
            out.write(lastnpwr)
            out.close()
        print ("Date are not same")

    return None


print "==== BEGIN ===="

#'CREATE TABLE psn (id INTEGER PRIMARY KEY, psnid VARCHAR(50), game VARCHAR(200), details VARCHAR(200), time VARCHAR(50), type VARCHAR(50))'
con = sqlite3.connect(DB_FILE, check_same_thread=False)

stored_trophies = {}

#list with stored psnids to check if new psnid was added in config
stored_psnids = []
tlgrm = {}

with con:
    cur = con.cursor()
    cur.execute("SELECT * FROM psn")
    rows = cur.fetchall()
    for row in rows:
        stored_trophies[row[1]] = (row[2], row[3], row[4], row[5])
        stored_psnids.append(row[1])
        if (row[6] is not None):
            tlgrm[row[1]] = row[6]

#pprint (stored_psnids)

for i in psnids:
    if i not in stored_psnids and not DEBUG:
        print "Adding new psnid (" + i + ") to DB"
        now = datetime.datetime.now()
        nowdate = now.strftime("%Y-%m-%dT%H:%M:%SZ")
        cur = con.cursor()
        cur.execute(
            'INSERT INTO psn (psnid, game, details, time, type, telegram) VALUES (?,?,?,?,?,?)',
            (i, '', '', nowdate, '', i)
            )
        con.commit()

        #adding new psnid to data dict from DB
        #so that we can compare its fake date '' with new date at once
        stored_trophies[i] = (i, '', nowdate, '', '')
        #pprint(stored_trophies[i])

        telegram ("<b>{}</b> добавлен".format(i))
        telegram ("<i>Для того, чтобы бот вывел ачивку в канал, нужно сразу при получении ачивки нажать кнопку PS и дождаться синхронизации, либо позже вручную зайти в ачивки через меню.</i>")
        telegram ("<i>Принято синхронизировать каждый трофей, а не вываливать в чат сразу много, за исключением случаев, когда трофеи падают одновременно.</i>")

pool = ThreadPool(2)

results = pool.map(last_trophy, psnids)
#last_trophy('gorcheque')
#last_trophy('nogpyraa')
#last_trophy('Nalorokk')
#last_trophy('gleb_miller')
pool.close()
pool.join()

print "==()()()()()()()=="
print "SUCCESS"
